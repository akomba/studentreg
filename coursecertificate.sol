contract CourseCertificate {

  // Unapproved & approved students
  mapping (address => string) studentNames;
  mapping (uint => address) unapproved;
  mapping (uint => address) approved;

  // Counters of student
  uint unapprovedCount;
  uint approvedCount;

  // Course name and owner address
  string public courseName;
  address owner;

  // Reward value for approved student
  uint64 reward;

  // Event logger
  event Issue(address indexed from, address indexed to);

  // Constructor
  function CourseCertificate(string name, uint64 amount) {
    // participant[msg.sender] = true;
    unapprovedCount = 0;
    approvedCount = 0;
    courseName = name;
    reward = amount;
    owner = msg.sender;
  }

  function SubmitStudent(string name) returns (bool res) {

    // Check name
    if (bytes(name).length < 1) {
      return false;
    }

    // Handle approved student
    if (IsApproved(msg.sender)) {
      return true;
    }

    // Handle unapproved student
    if (IsUnapproved(msg.sender)) {
      studentNames[msg.sender] = name;
      return true;
    }

    // Handle new student
    unapproved[unapprovedCount] = msg.sender;
    studentNames[msg.sender] = name;
    unapprovedCount += 1;
  }

  function ApproveStudent(address student) returns (bool res) {
    // Throw if caller is not owner
    if (owner != msg.sender) {
      return false;
    }

    // True if already approved
    if (IsApproved(student)) {
      return true;
    }

    // Check balance
    if (this.balance < reward) {
      return false;
    }

    // Mark approved if unapproved
    if (IsUnapproved(student)) {
      student.send(reward);
      approved[approvedCount] = student;
      approvedCount += 1;
      Issue(msg.sender, student);
    }

    // Otherwise false
    return false;
  }

  function GetCourseName() constant returns (string name) {
    return courseName;
  }

  function GetRewardAmount() constant returns (uint64 res) {
    return reward;
  }

  function GetStudentName(address student) constant returns (string name) {
    return studentNames[student];
  }

  function IsUnapproved(address student) constant returns (bool res) {
    for(uint i = 0; i < unapprovedCount; i++)
    {
      if (unapproved[i] == student) {
        return true;
      }
    }
    return false;
  }

  function IsApproved(address student) constant returns (bool res) {
    for(uint i = 0; i < approvedCount; i++)
    {
      if (approved[i] == student){
        return true;
      }
    }
    return false;
  }

  function GetUnapproved() constant returns (address[] unapprovedStudents) {

    // Collect unapproved students to array
    for(uint i = 0; i < unapprovedCount; i++)
    {
      address student = unapproved[i];
      if (IsApproved(student) == false) {
        unapprovedStudents[unapprovedStudents.length] = student;
      }
    }
    return unapprovedStudents;
  }

  function Withdrawal() returns (bool res) {

    // Is caller is owner
    if (msg.sender != owner) {
      return false;
    }

    // Check balance
    if (this.balance < 1) {
      return false;
    }

    // Send all funds to owner
    owner.send(this.balance);
    return true;
  }
}
